<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/* Questions Resource Route*/
Route::resource('question', 'QuestionController');

/* Questions Ajax Calls */
Route::get('api/question/all', 'QuestionController@apiGetQuestions');

Route::get('api/sort/questions', 'QuestionController@apiSortQuestions');

Route::get('api/question/changeenable', 'QuestionController@apiQuestionChangeEnable');

Route::get('crm/api/questions/childcount', 'QuestionController@apiQuestionChildCount');

Route::get('crm/api/questions/childresponse', 'QuestionController@apiQuestionChildResponse');

Route::get('crm/api/questions/childsort', 'QuestionController@apiQuestionChildSort');

Route::get('crm/api/questions/checkchild', 'QuestionController@apiQuestionChildCheck');

Route::get('crm/api/questions/getallactive', 'QuestionController@apiQuestionGetActive');