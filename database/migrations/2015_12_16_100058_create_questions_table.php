<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->text('question');
            $table->text('old_question');
            $table->string('postcoderestriction');
            $table->string('postcodeinclusion');
            $table->string('postcodeexclusion');
            $table->string('agerestriction');
            $table->string('agebracket');
            $table->string('ownhomerestriction');
            $table->string('ownhomeoptions');
            $table->string('telephonerestriction');
            $table->string('telephoneoptions');
            $table->string('columnheader');
            $table->string('deliveryassignment');
            $table->string('isenabled');
            $table->integer('sortorder');
            $table->float('costperlead');
            $table->float('costperlead_less');
            $table->string('po_num');
            $table->integer('is_child');
            $table->string('child_enable_response');
            $table->string('parent_colheader');
            $table->integer('child_count');
            $table->integer('child_sort_num');
            $table->string('parent_enable_response');
            $table->string('child_lead_respponse');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('questions');
    }
}
